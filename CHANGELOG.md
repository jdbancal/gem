## [0.1.1] - 2016-11-23
- Updated documentation
- Fixed issues when computing few eigenvalues of a small matrix
- Compiled with latest Spectra code
- Added changelog file

## [0.1.0] - 2016-11-19
- First release
- Implements basics matrix algebra for Sparse and dense matrices with arbitrary precision.
- Also implements simple functions like max/min, ... , some trigonometry, eigenvalues decompositions, singular values and basic linear systems solvers.

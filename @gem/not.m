% not - logical necation of 'logical'
function result = not(this)

result = ~logical(this);

end
